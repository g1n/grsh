use std::env;
use std::fs::{self, File};
use std::path::Path;

fn main() {
    let args: Vec<String> = env::args().collect();
    for arg in 1..args.len() {
        match args[arg].as_str() {
            "--help" => {
                println!("Usage: touch [OPTIONS] FILE");
                println!("Creates empty file");
                return;
            }
            arg => {
                if !Path::new(arg).exists() {
                    File::create(arg);
                }
            }
        }
    }
}
